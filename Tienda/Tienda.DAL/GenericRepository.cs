﻿using FluentValidation;
using FluentValidation.Results;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.DAL
{
    public class GenericRepository<T>:IGenericRepository<T> where T:BaseDTO 
    {
        MongoClient client;
        IMongoDatabase database;
        bool resultado;
        public string Error { get; private set; }
        AbstractValidator<T> validator;
        ValidationResult resultadoValidator;
        public GenericRepository(AbstractValidator<T> _validator)
        {

            client = new MongoClient("mongodb+srv://user:12345@tienda-jpbav.mongodb.net/test?retryWrites=true&w=majority");
            database = client.GetDatabase("Tienda");
            validator = _validator;

        }

        private IMongoCollection<T> Collection() => database.GetCollection<T>(typeof(T).Name);

        
        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    Error = "";
                    return Collection().AsQueryable();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public bool Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId();
            entidad.FechaHora = DateTime.Now;
            resultadoValidator = validator.Validate(entidad);
            if (resultadoValidator.IsValid)
            {
                try
                {
                    if (resultadoValidator.IsValid)
                    {
                        Collection().InsertOne(entidad);
                        Error = "";
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            else
            {
                Error = "Datos invalidos: ";
                foreach (var error in resultadoValidator.Errors)
                {
                    Error += $"\n>{error.ErrorMessage}";
                }
            }
            return resultado;
        }

        public bool Delete(ObjectId Id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(p => p.Id == Id).DeletedCount;
                resultado = r == 1;
                Error="";
            }
            catch (Exception ex)
            {

                Error = ex.Message;
                resultado = false;
            }
            return resultado;
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado) => Read.Where(predicado.Compile());

        public T SearchById(ObjectId Id) => Collection().Find(p=>p.Id==Id).SingleOrDefault();

        public bool Update(T entidad)
        {
            if (validator.Validate(entidad).IsValid)
            {
                try
                {
                    entidad.FechaHora = DateTime.Now;
                    int r = (int)Collection().ReplaceOne(p=>p.Id==entidad.Id, entidad).ModifiedCount;
                    resultado = r == 1;
                    Error = "";
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            return resultado;
        }
    }
}
