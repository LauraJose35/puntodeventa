﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class VentaManager : GenericManager<Ventas>, IVentaManager
    {
        public VentaManager(IGenericRepository<Ventas> repositorio) : base(repositorio)
        {
        }

        public IEnumerable<Ventas> VentaEnIntervalo(DateTime inicio, DateTime fin)
        {
            DateTime rInicio = new DateTime(inicio.Year, inicio.Month, inicio.Day, 0,0,0);
            DateTime rFin = new DateTime(fin.Year, fin.Month, fin.Day,0,0,0).AddDays(1);
            return repository.Query(p=>p.FechaHora>=rInicio && p.FechaHora<rFin);
        }

        public IEnumerable<Ventas> VentasDeClienteEnIntervalo(string nombreCliente, DateTime inicio, DateTime fin)
        {
            throw new NotImplementedException();
        }
    }
}
