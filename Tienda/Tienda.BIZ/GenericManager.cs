﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> repositorio)
        {
            repository = repositorio;
        }
        
        public string Error
        {
            get
            {
                return repository.Error;
            }
        }

        public IEnumerable<T> ObtenerDatos =>repository.Read; 

        public bool Actualizar(T entidad) => repository.Update(entidad);

        public T BuscarPorId(ObjectId Id) => repository.Read.Where(p=>p.Id==Id).SingleOrDefault();

        public bool Eliminar(T entidad) => repository.Delete(entidad.Id);

        public bool Insertar(T entidad) => repository.Create(entidad);
    }
}
