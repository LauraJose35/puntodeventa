﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Validadores;
using Tienda.DAL;

namespace Tienda.BIZ
{
    public class Factory
    {
        public CategoriaManager CategoriaManager() => new CategoriaManager(new GenericRepository<Categorias>(new CategoriaValidator()));
        public ClientesManager ClientesManager() => new ClientesManager(new GenericRepository<Clientes>(new ClienteValidator()));
        public EmpleadoManager EmpleadoManager() => new EmpleadoManager(new GenericRepository<Empleados>(new EmpleadoValidator()));
        public InventarioManager InventarioManager() => new InventarioManager(new GenericRepository<Inventarios>(new InventarioValidator()));
        public ProductoManager ProductoManager() => new ProductoManager(new GenericRepository<Productos>(new ProductoValidator()));
        public ProductoVendidoManager ProductoVendidoManager() => new ProductoVendidoManager(new GenericRepository<ProductoVendido>(new ProductoVendidoValidator()));
        public ProveedorManager ProveedorManager() => new ProveedorManager(new GenericRepository<Proveedores>(new ProveedorValidator()));
        public VentaManager VentaManager() => new VentaManager(new GenericRepository<Ventas>(new VentaValidator()));
    }
}
