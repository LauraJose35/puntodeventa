﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class CategoriaManager : GenericManager<Categorias>, ICategoriaManager
    {
        public CategoriaManager(IGenericRepository<Categorias> repositorio) : base(repositorio)
        {
        }
    }
}
