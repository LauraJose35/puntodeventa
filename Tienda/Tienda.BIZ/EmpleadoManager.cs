﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class EmpleadoManager : GenericManager<Empleados>, IEmpleadosManager
    {
        public EmpleadoManager(IGenericRepository<Empleados> repositorio) : base(repositorio)
        {
        }

        public Empleados Login(string nombreUsuario, string password)
        {
            return repository.Read.Where(p=>p.NombreUsuario==nombreUsuario && p.Password==password).SingleOrDefault();
        }
    }
}
