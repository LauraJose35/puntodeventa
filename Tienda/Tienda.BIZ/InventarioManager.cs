﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class InventarioManager : GenericManager<Inventarios>, IInventarioManager
    {
        public InventarioManager(IGenericRepository<Inventarios> repositorio) : base(repositorio)
        {
        }
    }
}
