﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class ProductoManager : GenericManager<Productos>, IProductoManager
    {
        public ProductoManager(IGenericRepository<Productos> repositorio) : base(repositorio)
        {
        }

        public Productos BuscarProductoPorNombreExacto(string nombre) => repository.Query(p=>p.Nombre==nombre).SingleOrDefault();

        public IEnumerable<Productos> BuscarProductosPorNombre(string criterio) => repository.Query(p=>p.Nombre.ToLower().Contains(criterio.ToLower()));
    }
}
