﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class ProductoVendidoManager : GenericManager<ProductoVendido>, IProductoVendidoManager
    {
        public ProductoVendidoManager(IGenericRepository<ProductoVendido> repositorio) : base(repositorio)
        {
        }              
    }
}
