﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class ClientesManager : GenericManager<Clientes>, IClientesManager
    {
        public ClientesManager(IGenericRepository<Clientes> repositorio) : base(repositorio)
        {
        }
    }
}
