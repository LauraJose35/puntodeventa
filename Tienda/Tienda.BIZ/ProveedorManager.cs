﻿using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class ProveedorManager : GenericManager<Proveedores>, IProveedorManager
    {
        public ProveedorManager(IGenericRepository<Proveedores> repositorio) : base(repositorio)
        {
        }

        public ObjectId BuscarId(string nombreCompleto)
        {            
            Proveedores proveedores = repository.Read.Where(p=>(p.NombreCompleto+" "+ p.Apellidos)==nombreCompleto).FirstOrDefault();
            return proveedores.Id;
        }

        public IEnumerable ObtenerNombre(ObjectId id_Proveedores)
        {
            Proveedores proveedores = repository.Read.Where(p =>p.Id == id_Proveedores).FirstOrDefault();
            string nombreCompleto= proveedores.NombreCompleto + " " + proveedores.Apellidos;
            return nombreCompleto.ToString();
        }
    }
}
