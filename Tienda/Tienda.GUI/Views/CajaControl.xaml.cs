﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;
using MongoDB.Bson;
using System.Collections.ObjectModel;
using System.ComponentModel;


namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para CajaControl.xaml
    /// </summary>
    public partial class CajaControl : UserControl
    {
        IProductoManager managerProducto;
        IClientesManager clientesManager;
        IProductoVendidoManager productoVendidoManager;
        IVentaManager ventaManager;
        int valor = 0;
        float Total = 0;
        float  Cambio=0;
        List<ProductosVendidosCaja> productosVendidos;
        Empleados empleados;
        public CajaControl(Empleados empleados)
        {
            InitializeComponent();
            managerProducto = App.Factory.ProductoManager();
            clientesManager = App.Factory.ClientesManager();
            productoVendidoManager = App.Factory.ProductoVendidoManager();
            ventaManager = App.Factory.VentaManager();
            ActualizarTabla();
            productosVendidos = new List<ProductosVendidosCaja>();
            this.empleados = empleados;
        }

        private void ActualizarTabla()
        {
            cmbProducto.ItemsSource = null;
            cmbCliente.ItemsSource = null;

            cmbProducto.ItemsSource = managerProducto.ObtenerDatos.OrderBy(p=>p.Nombre);
            cmbCliente.ItemsSource = clientesManager.ObtenerDatos.OrderBy(p=>p.NombreCompleto);
        }

        private void btnAgregarProducto_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtCantidad.Text))
            {
                try
                {
                    valor = int.Parse(txtCantidad.Text);
                    if (valor > 0)
                    {
                        Productos productos = cmbProducto.SelectedItem as Productos;
                        if (productos != null)
                        {
                            productosVendidos.Add(new ProductosVendidosCaja()
                            {
                                Cantidad_Producto = valor,
                                Id_Producto = productos.Id,
                                Nombre_Producto = productos.Nombre,
                                Importe = productos.Precio * valor,
                                Precio_Producto = productos.Precio
                            });
                            txtCantidad.Clear();
                            RealizarSumaCaja();
                        }
                        else
                        {
                            App.MensajeError("Seleccione primero un producto");
                        }
                    }
                }
                catch (Exception ex)
                {
                    App.MensajeError(ex.Message);
                }
            }
            else
            {
                App.MensajeError("Falta agregar el número de productos a adquirir");
            }              
        }


        private void btnTerminarVenta_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Ventas ventas = new Ventas()
                {
                    Cambio = Cambio,
                    CantidadArticulos = productosVendidos.Count,
                    NombreEmpleado = empleados.NombreCompleto + " " + empleados.Apellidos,
                    MontoTotal = Total,
                    ProductosVendidos = productosVendidos
                };
                if (ventaManager.Insertar(ventas))
                {
                    App.MensajeInformacion("Venta terminada");
                    listProductosPorVender.ItemsSource = null;
                    LimpiarCampos();
                }
                else
                {
                    App.MensajeError("Primero llena la casilla Pago con efectivo");
                }
            }
            catch (Exception)
            {
                App.MensajeError("Primero llena la casilla Pago con efectivo");
            }                
        }

        private void btnBorrarSeleccionado_Click(object sender, RoutedEventArgs e)
        {
            ProductosVendidosCaja ProductosVendidosCaja = listProductosPorVender.SelectedItem as ProductosVendidosCaja;
            if (App.MensajePregunta("Esta seguro de eliminar este producto") == MessageBoxResult.Yes)
            {
                productosVendidos.Remove(ProductosVendidosCaja);
                RealizarSumaCaja();
            }
        }

        private void RealizarSumaCaja()
        {
            
            foreach (var item in productosVendidos)
            {
                Total += item.Importe;
            }
            txtCantidadProductos.Content = productosVendidos.Count;
            txtTotal.Content = Total;
            //
            listProductosPorVender.ItemsSource = null;
            listProductosPorVender.ItemsSource = productosVendidos;
        }

        private void btnCambio_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPago.Text))
            {
                Cambio = float.Parse(txtPago.Text) - float.Parse(Total.ToString());
                txtRestan.Content = Cambio;
            }
        }

        private void btnCancelarVenta_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCampos();            
        }

        private void LimpiarCampos()
        {
            txtCantidad.Clear();
            txtCantidadProductos.Content = "";
            txtPago.Clear();
            txtRestan.Content = "";
            txtTotal.Content = "";
            listProductosPorVender.ItemsSource = null;
            productosVendidos = new List<ProductosVendidosCaja>();
        }
    }
}
