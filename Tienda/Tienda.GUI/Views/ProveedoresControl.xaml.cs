﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para ProveedoresControl.xaml
    /// </summary>
    public partial class ProveedoresControl : UserControl
    {
        IProveedorManager proveedor;
        bool esNuevo;
        public ProveedoresControl()
        {
            InitializeComponent();
            proveedor = App.Factory.ProveedorManager();
            esNuevo = true;
            habilitarCajas(false);
            this.DataContext = new Proveedores();
            ActualizarTabla();
        }
        private void habilitarCajas(bool v)
        {
            ContenedorCampos.IsEnabled = v;
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnCancelar.IsEnabled = v;
        }

        private void ActualizarTabla()
        {
            this.DataContext = new Proveedores();
            lblContador.Content = " Total: "+proveedor.ObtenerDatos.Count();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = proveedor.ObtenerDatos.OrderBy(p => p.Apellidos).OrderBy(e => e.NombreCompleto);
            
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Proveedores();
            habilitarCajas(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevo)
            {

                Proveedores proveedores = this.DataContext as Proveedores;
                if (proveedor.Insertar(proveedores))
                {
                    App.MensajeInformacion("Proveedores agregado correctamente");
                    ActualizarTabla();
                }
                else
                    App.MensajeError(proveedor.Error);
            }
            else
            {
                Proveedores proveedores = this.DataContext as Proveedores;
                if (proveedor.Actualizar(proveedores))
                {
                    App.MensajeInformacion("Proveedores actualizado correctamente");
                    ActualizarTabla();
                }
                else
                    App.MensajeError(proveedor.Error);
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Proveedores proveedores = dtgDatos.SelectedItem as Proveedores;
            if (proveedores != null)
            {
                this.DataContext = proveedores;
                esNuevo = false;
                habilitarCajas(true);
            }
            else
            {
                App.MensajeError("Seleccione un proveedor");
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Proveedores proveedores = dtgDatos.SelectedItem as Proveedores;
            if (proveedores != null)
            {
                if (App.MensajePregunta("Realmente esta seguro de eliminar el proveedor: " + proveedores.NombreCompleto) == MessageBoxResult.Yes)
                {
                    proveedor.Eliminar(proveedores);
                    ActualizarTabla();
                }
            }
            else
            {
                App.MensajeError("Seleccione un producto");
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Proveedores();
            habilitarCajas(false);
        }
    }
}
