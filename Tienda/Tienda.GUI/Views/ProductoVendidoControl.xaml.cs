﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para ProductoVendidoControl.xaml
    /// </summary>
    public partial class ProductoVendidoControl : UserControl
    {
        IVentaManager managerVentas;
        IProductoVendidoManager productovendido;
        public ProductoVendidoControl()
        {
            InitializeComponent();
            managerVentas = App.Factory.VentaManager();
            productovendido = App.Factory.ProductoVendidoManager();
            ActualizarTabla();
            
        }

        private void ActualizarTabla()
        {
            List<Ventas> ventas = new List<Ventas>();
            ventas = managerVentas.ObtenerDatos.ToList();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = managerVentas.ObtenerDatos.OrderBy(p => p.FechaHora);
        }
                
        private void dtgDatos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Ventas ventas = dtgDatos.SelectedItem as Ventas;
            if (ventas != null)
            {
                dtgDatosColumna.ItemsSource = null;
                dtgDatosColumna.ItemsSource = ventas.ProductosVendidos;
            }
            else
            {
                App.MensajeError("Seleccione una columna");
            }
        }
    }
}
