﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        IEmpleadosManager empleadosManager;
        public Login()
        {
            InitializeComponent();
            empleadosManager = App.Factory.EmpleadoManager();
        }

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Empleados empleados = empleadosManager.Login(txtNombreUsuario.Text, txtContrasena.Password);
                if (empleados != null)
                {
                    App.MensajeInformacion("Bienvenido " + empleados.NombreUsuario);
                    Menu menu = new Menu(empleados);
                    menu.Show();
                    this.Close();
                }
                else
                {
                    App.MensajeError("Usuario o contraseña incorrecto");
                }
            }
            catch (Exception ex)
            {
                App.MensajeError(ex.Message);
            }
         
        }

        private void btnCerrarVentana_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
