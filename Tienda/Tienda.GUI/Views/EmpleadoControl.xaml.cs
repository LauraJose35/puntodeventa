﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para EmpleadoControl.xaml
    /// </summary>
    public partial class EmpleadoControl : UserControl
    {
        IEmpleadosManager empleados;
        bool esNuevo;
        public EmpleadoControl()
        {
            InitializeComponent();
            empleados = App.Factory.EmpleadoManager();
            esNuevo = true;
            habilitarCajas(false);
            this.DataContext = new Empleados();
            ActualizarTabla();
        }
        private void habilitarCajas(bool v)
        {
            ContenedorCampos.IsEnabled = v;
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnCancelar.IsEnabled = v;
        }

        private void ActualizarTabla()
        {
            this.DataContext = new Empleados();
            lblContador.Content = " Total: "+ empleados.ObtenerDatos.Count();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = empleados.ObtenerDatos.OrderBy(p => p.Apellidos).OrderBy(e => e.NombreCompleto);
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Empleados();
            habilitarCajas(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevo)
            {

                Empleados Empleados = this.DataContext as Empleados;
                if (empleados.Insertar(Empleados))
                {
                    App.MensajeInformacion("Empleados agregado correctamente");
                    ActualizarTabla();
                    habilitarCajas(false);
                }
                else                
                    App.MensajeError(empleados.Error);                   
            }
            else
            {
                Empleados Empleados = this.DataContext as Empleados;
                if (empleados.Actualizar(Empleados))
                {
                    App.MensajeInformacion("Empleados actualizado correctamente");
                    ActualizarTabla();
                    habilitarCajas(false);
                }
                else
                    App.MensajeError(empleados.Error);
            }            
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Empleados Empleados = dtgDatos.SelectedItem as Empleados;
            if (Empleados != null)
            {
                this.DataContext = Empleados;
                esNuevo = false;
                habilitarCajas(true);
            }
            else
            {
                App.MensajeError("Seleccione un empleado");
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Empleados Empleados = dtgDatos.SelectedItem as Empleados;
            if (Empleados != null)
            {
                if (App.MensajePregunta("Realmente esta seguro de eliminar el empleado: " + Empleados.NombreCompleto) == MessageBoxResult.Yes)
                {
                    empleados.Eliminar(Empleados);
                    ActualizarTabla();
                }
            }
            else
            {
                App.MensajeError("Seleccione un empleado");
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Empleados();
            habilitarCajas(false);
        }
    }
}