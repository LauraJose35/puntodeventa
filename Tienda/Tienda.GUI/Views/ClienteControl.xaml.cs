﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para ClienteControl.xaml
    /// </summary>
    public partial class ClienteControl : UserControl
    {
        IClientesManager clientes;
        bool esNuevo;
        public ClienteControl()
        {
            InitializeComponent();
            clientes = App.Factory.ClientesManager();
            esNuevo = true;
            habilitarCajas(false);
            this.DataContext = new Clientes();
            ActualizarTabla();
        }
        private void habilitarCajas(bool v)
        {
            ContenedorCampos.IsEnabled = v;
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnCancelar.IsEnabled = v;
        }

        private void ActualizarTabla()
        {
            this.DataContext = new Clientes();
            lblContador.Content = " Total: "+ clientes.ObtenerDatos.Count();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = clientes.ObtenerDatos.OrderBy(p => p.Apellidos).OrderBy(e => e.NombreCompleto);
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Clientes();
            habilitarCajas(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevo)
            {
                Clientes Clientes = this.DataContext as Clientes;
                if (clientes.Insertar(Clientes))
                {
                    App.MensajeInformacion("Clientes agregado correctamente");
                    ActualizarTabla();
                }
                else
                    App.MensajeError(clientes.Error);
            }
            else
            {
                Clientes Clientes = this.DataContext as Clientes;
                if (clientes.Actualizar(Clientes))
                {
                    App.MensajeInformacion("Clientes actualizado correctamente");
                    ActualizarTabla();
                    habilitarCajas(false);
                }
                else
                    App.MensajeError(clientes.Error);
            }
           
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Clientes Clientes = dtgDatos.SelectedItem as Clientes;
            if (Clientes != null)
            {
                this.DataContext = Clientes;
                esNuevo = false;
                habilitarCajas(true);
            }
            else
            {
                App.MensajeError("Seleccione un clientes");
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Clientes Clientes = dtgDatos.SelectedItem as Clientes;
            if (Clientes != null)
            {
                if (App.MensajePregunta("Realmente esta seguro de eliminar el cliente: " + Clientes.NombreCompleto) == MessageBoxResult.Yes)
                {
                    clientes.Eliminar(Clientes);
                    ActualizarTabla();
                }
            }
            else
            {
                App.MensajeError("Seleccione un cliente");
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Clientes();
            habilitarCajas(false);
        }
    }
}