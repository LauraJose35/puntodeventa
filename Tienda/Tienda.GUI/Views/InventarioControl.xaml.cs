﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para Inventariocontrol.xaml
    /// </summary>
    public partial class InventarioControl : UserControl
    {
        IInventarioManager inventarios;
        IProductoManager Productos;
        ICategoriaManager categorias;
        bool esNuevo;
        public InventarioControl()
        {
            InitializeComponent();
            inventarios = App.Factory.InventarioManager();
            Productos = App.Factory.ProductoManager();
            categorias = App.Factory.CategoriaManager();
            esNuevo = true;
            habilitarCajas(false);
            this.DataContext = new Inventarios();
            ActualizarTabla();
        }
        private void habilitarCajas(bool v)
        {
            ContenedorCampos.IsEnabled = v;
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnCancelar.IsEnabled = v;
        }

        private void ActualizarTabla()
        {
            this.DataContext = new Inventarios();
            lblContador.Content = " Total: "+inventarios.ObtenerDatos.Count();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = inventarios.ObtenerDatos.OrderBy(p=>p.Producto.Nombre);

            cmbProducto.ItemsSource = null;
            cmbProducto.ItemsSource = Productos.ObtenerDatos.OrderBy(p=>p.Nombre);

            cmbCategoria.ItemsSource = null;
            cmbCategoria.ItemsSource = categorias.ObtenerDatos.OrderBy(p => p.Nombre_Categoria);


        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Inventarios();
            habilitarCajas(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevo)
            {
                Inventarios Inventarios = this.DataContext as Inventarios;
                Inventarios.Producto = cmbProducto.SelectedItem as Productos;
                if (inventarios.Insertar(Inventarios))
                {
                    App.MensajeInformacion("Inventarios agregado correctamente");
                    habilitarCajas(false);
                }
                else
                    App.MensajeError(inventarios.Error);
            }
            else
            {
                Inventarios Inventarios = this.DataContext as Inventarios;
                if (inventarios.Actualizar(Inventarios))
                {
                    App.MensajeInformacion("Inventarios actualizado correctamente");
                    habilitarCajas(false);
                }

                else
                    App.MensajeError(inventarios.Error);
            }
            ActualizarTabla();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Inventarios Inventarios = dtgDatos.SelectedItem as Inventarios;
            if (Inventarios != null)
            {
                this.DataContext = Inventarios;
                cmbProducto.Text = Inventarios.Producto.Nombre+ " "+Inventarios.Producto.ContenidoNeto;
                esNuevo = false;
                habilitarCajas(true);
            }
            else
            {
                App.MensajeError("Seleccione un inventarios");
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Inventarios Inventarios = dtgDatos.SelectedItem as Inventarios;
            if (Inventarios != null)
            {
                if (App.MensajePregunta("Realmente esta seguro de eliminar el inventario: " + Inventarios.Producto) == MessageBoxResult.Yes)
                {
                    inventarios.Eliminar(Inventarios);
                    ActualizarTabla();
                }
            }
            else
            {
                App.MensajeError("Seleccione un inventario");
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Inventarios();
            habilitarCajas(false);
        }
    }
}