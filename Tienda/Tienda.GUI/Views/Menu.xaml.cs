﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        Empleados _Empleados;
        public Menu(Empleados empleados)
        {
            InitializeComponent();
            this._Empleados = empleados;
            lblUsuario.Content = $"{_Empleados.NombreCompleto} {_Empleados.Apellidos}";
        }
        
        private void menuEmpleado_Selected(object sender, RoutedEventArgs e)
        {
            nuevosControl(new EmpleadoControl());
        }

        private void nuevosControl(Control control)
        {
            userControl.Content = null;
            userControl.Content = control;
        }

        private void menuProductos_Selected(object sender, RoutedEventArgs e)
        {
            nuevosControl(new ProductosUserControl());
        }

        private void menuProveedores_Selected(object sender, RoutedEventArgs e)
        {
            nuevosControl(new ProveedoresControl());
        }

        private void menuCliente_Selected(object sender, RoutedEventArgs e)
        {
            nuevosControl(new ClienteControl());
        }

        private void menuProductoVendido_Selected(object sender, RoutedEventArgs e)
        {
            nuevosControl(new ProductoVendidoControl());
        }

        private void menuInventario_Selected(object sender, RoutedEventArgs e)
        {
            nuevosControl(new InventarioControl());
        }

        private void menuCaja_Selected(object sender, RoutedEventArgs e)
        {
            nuevosControl(new CajaControl(_Empleados));
        }
    }
}
