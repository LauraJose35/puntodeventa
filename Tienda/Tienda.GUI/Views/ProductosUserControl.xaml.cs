﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.GUI.Views
{
    /// <summary>
    /// Lógica de interacción para PrincipalUserControl.xaml
    /// </summary>
    public partial class ProductosUserControl : UserControl
    {
        IProductoManager producto;
        IProveedorManager proveedor;
        ICategoriaManager categoria;
        bool esNuevo;
        bool esNuevaCategoria;
        public ProductosUserControl()
        {
            InitializeComponent();
            
            producto = App.Factory.ProductoManager();
            proveedor = App.Factory.ProveedorManager();
            categoria = App.Factory.CategoriaManager();
            
            esNuevo = true;
            esNuevaCategoria = true;
            
            habilitarCajas(false);
            habilitarCajasCategoria(false);
           
            this.DataContext = new Productos();            
            ActualizarTabla();
            ActualizarTablaCategoria();

           
        }

        #region Botones Productos

        private void habilitarCajas(bool v)
        {
            ContenedorCampos.IsEnabled = v;
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnCancelar.IsEnabled = v;
        }

        private void ActualizarTabla()
        {
            this.DataContext = new Productos();
            lblContador.Content = " Total: " + producto.ObtenerDatos.Count();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = producto.ObtenerDatos.OrderBy(e => e.Nombre);

            cmbCategoria.ItemsSource = null;
            cmbCategoria.ItemsSource = categoria.ObtenerDatos.OrderBy(p=>p.Nombre_Categoria);

            cmbProveedor.ItemsSource = null;
            cmbProveedor.ItemsSource = proveedor.ObtenerDatos.OrderBy(p=>p.Apellidos).OrderBy(e=>e.NombreCompleto);

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Productos();
            habilitarCajas(true);

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevo)
            {
                Productos productos = this.DataContext as Productos;
                if (!string.IsNullOrEmpty(cmbProveedor.Text))
                {
                    productos.Id_Proveedores = proveedor.BuscarId(cmbProveedor.Text);
                }
                productos.Categoria = cmbCategoria.Text;
                productos.FechaCaducidad = dtgFechaCaducidad.DisplayDate;
                if (producto.Insertar(productos))
                {
                    App.MensajeInformacion("Producto agregado correctamente");
                    ActualizarTabla();
                    habilitarCajas(false);
                }
                else
                    App.MensajeError(producto.Error);
            }
            else
            {
                Productos productos = this.DataContext as Productos;
                if (!string.IsNullOrEmpty(cmbProveedor.Text))
                {
                    productos.Id_Proveedores = proveedor.BuscarId(cmbProveedor.Text);
                }
                productos.Categoria = cmbCategoria.Text;
                if (producto.Actualizar(productos))
                {
                    App.MensajeInformacion("Producto actualizado correctamente");
                    ActualizarTabla();
                    habilitarCajas(false);
                }
                else
                    App.MensajeError(producto.Error);
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Productos productos = dtgDatos.SelectedItem as Productos;
            if (productos != null)
            {
                this.DataContext = productos;
                cmbProveedor.Text= proveedor.ObtenerNombre(productos.Id_Proveedores).ToString();
                esNuevo = false;
                habilitarCajas(true);
            }
            else
            {
                App.MensajeError("Seleccione un producto");
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Productos productos = dtgDatos.SelectedItem as Productos;
            if (productos != null)
            {
                if (App.MensajePregunta("Realmente esta seguro de eliminar el producto: " + productos.Nombre) == MessageBoxResult.Yes)
                {
                    producto.Eliminar(productos);
                    ActualizarTabla();
                }
            }
            else
            {
                App.MensajeError("Seleccione un producto");
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Productos();
            habilitarCajas(false);
        }

        #endregion

        #region Botones Categoria
        private void habilitarCajasCategoria(bool v)
        {
            ContenedorCategoria.IsEnabled = v;
            btnNuevoCategoria.IsEnabled = !v;
            btnEditarCategoria.IsEnabled = !v;
            btnGuardarCategoria.IsEnabled = v;
            btnEliminarCategoria.IsEnabled = !v;
            btnCancelarCategoria.IsEnabled = v;
        }

        private void ActualizarTablaCategoria()
        {
            lblContadorCategoria.Content = " Total: " + categoria.ObtenerDatos.Count();
            dtgDatosCategoria.ItemsSource = null;
            dtgDatosCategoria.ItemsSource = categoria.ObtenerDatos.OrderBy(p => p.Nombre_Categoria);

            cmbCategoria.ItemsSource = null;
            cmbCategoria.ItemsSource = categoria.ObtenerDatos.OrderBy(p => p.Nombre_Categoria);
        }

        private void btnNuevoCategoria_Click(object sender, RoutedEventArgs e)
        {
            esNuevaCategoria = true;
            habilitarCajasCategoria(true);
        }

        private void btnGuardarCategoria_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevaCategoria)
            {
                Categorias categorias = new Categorias() { Nombre_Categoria=txtCategoria.Text };
                if (categoria.Insertar(categorias))
                {
                    App.MensajeInformacion("Categoria agregado correctamente");
                    ActualizarTablaCategoria();
                    habilitarCajasCategoria(false);
                }
                else
                    App.MensajeError(categoria.Error);
            }
            else
            {
                Categorias categorias = dtgDatosCategoria.SelectedItem as Categorias;
                categorias.Nombre_Categoria =txtCategoria.Text;
                if (categoria.Actualizar(categorias))
                {
                    App.MensajeInformacion("Categoria actualizado correctamente");
                    ActualizarTablaCategoria();
                    habilitarCajasCategoria(false);
                }
                else
                    App.MensajeError(categoria.Error);
            }
        }

        private void btnEditarCategoria_Click(object sender, RoutedEventArgs e)
        {
            Categorias categorias = dtgDatosCategoria.SelectedItem as Categorias;
            if (categorias != null)
            {
                txtCategoria.Text = categorias.Nombre_Categoria;
                esNuevaCategoria = false;
                habilitarCajasCategoria(true);
            }
            else
            {
                App.MensajeError("Seleccione una categoria");
            }
        }

        private void btnEliminarCategoria_Click(object sender, RoutedEventArgs e)
        {
            Categorias categorias = dtgDatosCategoria.SelectedItem as Categorias;
            if (categorias != null)
            {
                if (App.MensajePregunta("Realmente esta seguro de eliminar la categoria: " + categorias.Nombre_Categoria) == MessageBoxResult.Yes)
                {
                    categoria.Eliminar(categorias);
                    ActualizarTablaCategoria();
                }
            }
            else
            {
                App.MensajeError("Seleccione una categoria");
            }
        }

        private void btnCancelarCategoria_Click(object sender, RoutedEventArgs e)
        {
            esNuevaCategoria = true;
            habilitarCajasCategoria(false);
        } 
        #endregion
    }
}
