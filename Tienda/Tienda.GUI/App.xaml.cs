﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Tienda.BIZ;
using Microsoft.VisualBasic;
namespace Tienda.GUI
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Factory Factory =>new Factory();

        #region Mensajes

        public static void MensajeError(string Error) => MessageBox.Show(Error, "Punto de venta", MessageBoxButton.OK, MessageBoxImage.Error);
        public static void MensajeInformacion(string Informacion) => MessageBox.Show(Informacion, "Punto de venta", MessageBoxButton.OK, MessageBoxImage.Information);
        public static MessageBoxResult MensajePregunta(string Pregunta) => MessageBox.Show(Pregunta, "Punto de venta", MessageBoxButton.YesNo, MessageBoxImage.Question);
        #endregion
    }
}
