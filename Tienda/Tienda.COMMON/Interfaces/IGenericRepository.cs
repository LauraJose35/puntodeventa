﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        string Error { get; }
        bool Create(T entidad);
        IEnumerable<T> Read { get; }
        bool Update(T entidad);
        bool Delete(ObjectId Id);
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
        T SearchById(ObjectId Id);
    }
}
