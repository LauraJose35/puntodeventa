﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IProductoManager:IGenericManager<Productos>
    {
        IEnumerable<Productos> BuscarProductosPorNombre(string criterio);
        Productos BuscarProductoPorNombreExacto(string nombre);
    }
}
