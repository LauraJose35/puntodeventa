﻿using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IProveedorManager : IGenericManager<Proveedores>
    {
        ObjectId BuscarId(string nombreCompleto);
        IEnumerable ObtenerNombre(ObjectId id_Proveedores);
    }
}
