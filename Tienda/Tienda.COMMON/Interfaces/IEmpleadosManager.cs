﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IEmpleadosManager:IGenericManager<Empleados>
    {
        Empleados Login(string nombreUsuario, string password);
    }
}
