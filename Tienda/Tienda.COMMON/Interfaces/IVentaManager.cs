﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IVentaManager:IGenericManager<Ventas>
    {
        IEnumerable<Ventas> VentaEnIntervalo(DateTime inicio, DateTime fin);
        IEnumerable<Ventas> VentasDeClienteEnIntervalo(string nombreCliente, DateTime inicio, DateTime fin);
    }
}
