﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        string Error { get; }
        IEnumerable<T> ObtenerDatos { get; }
        bool Actualizar(T entidad);
        bool Eliminar(T entidad);
        bool Insertar(T entidad);
        T BuscarPorId(ObjectId Id);
    }
}
