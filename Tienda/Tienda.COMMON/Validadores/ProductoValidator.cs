﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class ProductoValidator:AbstractValidator<Productos>
    {
        public ProductoValidator()
        {
            RuleFor(p=>p.Precio).NotNull().GreaterThan(0);
            RuleFor(p=>p.Nombre).NotNull().NotEmpty().Length(1, 50);
            RuleFor(p=>p.Categoria).NotNull().NotEmpty();
            RuleFor(p=>p.CodigoBarras).NotNull().NotEmpty();
            RuleFor(p=>p.ContenidoNeto).NotNull().NotEmpty();
            RuleFor(p=>p.FechaCaducidad).NotEmpty().NotNull();
            RuleFor(p=>p.Id_Proveedores).NotEmpty().NotNull();
        }
    }
}
