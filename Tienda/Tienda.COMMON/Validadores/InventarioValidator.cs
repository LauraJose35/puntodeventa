﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class InventarioValidator:AbstractValidator<Inventarios>
    {
        public InventarioValidator()
        {
            RuleFor(p=>p.Producto).NotEmpty().NotNull();
            RuleFor(p=>p.Stock).NotNull().NotEmpty();
            RuleFor(p=>p.Tipo).NotEmpty().NotNull();
        }
    }
}
