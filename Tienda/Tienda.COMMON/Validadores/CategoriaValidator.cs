﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class CategoriaValidator:AbstractValidator<Categorias>
    {
        public CategoriaValidator()
        {
            RuleFor(p=>p.Nombre_Categoria).NotNull().NotEmpty().Length(1,50);
        }
    }
}
