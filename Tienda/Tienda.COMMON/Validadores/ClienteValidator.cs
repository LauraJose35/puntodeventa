﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class ClienteValidator:AbstractValidator<Clientes>
    {
        public ClienteValidator()
        {
            RuleFor(p=>p.Apellidos).NotEmpty().NotNull().Length(1,50);
            RuleFor(p=>p.Correo).NotEmpty().NotNull().EmailAddress();
            RuleFor(p=>p.NombreCompleto).NotNull().NotEmpty().Length(1,50);
            RuleFor(p=>p.RFC).NotEmpty().NotNull().Length(1, 50);
            RuleFor(p=>p.Telefono).NotNull().NotEmpty();
        }
    }
}
