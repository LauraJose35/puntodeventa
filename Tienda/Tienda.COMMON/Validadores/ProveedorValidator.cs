﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class ProveedorValidator:AbstractValidator<Proveedores>
    {
        public ProveedorValidator()
        {
            RuleFor(p => p.Apellidos).NotNull().NotEmpty().Length(1,50);
            RuleFor(p => p.Correo).NotNull().NotEmpty().EmailAddress();
            RuleFor(p => p.Empresa).NotNull().NotEmpty();
            RuleFor(p => p.NombreCompleto).NotNull().NotEmpty().Length(1,50);
            RuleFor(p => p.Telefono).NotNull().NotEmpty();
        }
    }
}
