﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class VentaValidator:AbstractValidator<Ventas>
    {
        public VentaValidator()
        {
            RuleFor(p=>p.FechaHora).NotNull().NotEmpty();
            RuleFor(p=>p.Cambio).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(p=>p.CantidadArticulos).NotNull().NotEmpty();
            RuleFor(p => p.NombreEmpleado).NotNull().NotEmpty();
            RuleFor(p => p.MontoTotal).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(p => p.ProductosVendidos).NotNull().NotEmpty();
        }
    }
}
