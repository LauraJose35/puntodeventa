﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class EmpleadoValidator: AbstractValidator<Empleados>
    {
        public EmpleadoValidator()
        {
            RuleFor(p=>p.Apellidos).NotNull().NotEmpty().Length(1, 50);
            RuleFor(p=>p.Correo).NotNull().NotEmpty().EmailAddress();
            RuleFor(p=>p.NombreUsuario).NotNull().NotEmpty().Length(1, 50);
            RuleFor(p=>p.Password).NotEmpty().NotNull().Length(1, 50);
            RuleFor(p => p.NombreCompleto).NotEmpty().NotNull().Length(1, 50);
            RuleFor(p=>p.Telefono).NotNull().NotEmpty();
            RuleFor(p=>p.TipoEmpleado).NotNull().NotEmpty().Length(1,50);
        }
    }
}
