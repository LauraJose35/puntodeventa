﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.GUI.Models
{
    public class CamposModelModel
    {
        public string NombreCampo { get; set; }
        public string BindingPropiedad { get; set; }
        public string TipoDato { get; set; }
    }
}
