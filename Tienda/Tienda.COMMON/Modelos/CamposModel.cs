﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Modelos
{
    public class CamposModel
    {
        public string NombrePropiedad { get; set; }
        public string TipoPropiedad { get; set; }
        public string NombreCampo { get; set; }
    }
}
