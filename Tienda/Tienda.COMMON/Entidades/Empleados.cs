﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Empleados:Persona
    {
        public string Password { get; set; }
        public string NombreUsuario { get; set; }
        public string TipoEmpleado { get; set; }
    }
}
