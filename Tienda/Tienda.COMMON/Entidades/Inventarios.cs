﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Inventarios:BaseDTO
    {
        public Productos Producto { get; set; }
        public string Tipo { get; set; }
        public int Stock { get; set; }
    }
}
