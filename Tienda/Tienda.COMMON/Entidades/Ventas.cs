﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Ventas:BaseDTO
    {
        public List<ProductosVendidosCaja> ProductosVendidos { get; set; }
        public int CantidadArticulos { get; set; }
        public float MontoTotal { get; set; }
        public float Cambio { get; set; }
        public string NombreEmpleado { get; set; }
        //[BsonId]
       // public string Codigo { get; set; }
    }
}
