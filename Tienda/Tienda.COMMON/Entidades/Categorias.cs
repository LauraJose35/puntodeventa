﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Categorias:BaseDTO
    {
        public string Nombre_Categoria { get; set; }
        public override string ToString()
        {
            return Nombre_Categoria;
        }
    }
}
