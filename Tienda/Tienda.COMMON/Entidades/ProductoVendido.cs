﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class ProductoVendido:BaseDTO
    {
        public int Cantidad { get; set; }
        public decimal Total { get; set; }
        public decimal Pago { get; set; }
        public decimal Cambio { get; set; }
        public List<ProductosVendidosCaja> productos { get; set; }
    }
}
