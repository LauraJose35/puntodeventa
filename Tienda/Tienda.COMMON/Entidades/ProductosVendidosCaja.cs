﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class ProductosVendidosCaja
    {
        public ObjectId Id_Producto { get; set; }
        public string Nombre_Producto { get; set; }
        public float Cantidad_Producto { get; set; }
        public float Precio_Producto { get; set; }
        public float Importe { get; set; }
    }
}
