﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Proveedores: Persona
    {
        public string Empresa { get; set; }
        public override string ToString()
        {
            return NombreCompleto + " " + Apellidos;
        }
    }
}
