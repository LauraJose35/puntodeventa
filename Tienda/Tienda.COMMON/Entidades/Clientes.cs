﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Clientes: Persona
    {
        public string RFC { get; set; }
        public override string ToString()
        {
            return NombreCompleto + " " + Apellidos;
        }
    }
}
