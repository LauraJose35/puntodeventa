﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Productos:BaseDTO
    {
        public string Nombre { get; set; }
        public string Categoria { get; set; }
        public DateTime FechaCaducidad { get; set; }
        public float Precio { get; set; }
        public string ContenidoNeto { get; set; }
        public ObjectId Id_Proveedores { get; set; }
        public string CodigoBarras { get; set; }
        public override string ToString()
        {
            return Nombre + " "+ContenidoNeto;
        }
    }
}
